# docker-cd-ci

Docker and CD-CI
This folder contains material related to a seminar regarding docker and cd-ci organised for Cloud and Edge computing course

## repo branch details

### main
This branch contain this .md only for now

### dockerfile-basis
This branch contains:
- docker basic instruction notes
- simple empty django project
- simple Dockefile for running django

### exercise1
This branch contains:
- simple empty django project
- exercise 1 text
- exercise 1 solution


### exercise1-adv
This branch contains:
- simple empty django project
- exercise 1 advanced version text
